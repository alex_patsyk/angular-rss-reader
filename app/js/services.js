rssReaderApp.factory('feedService', ['$http', '$rootScope', function($http, $rootScope) {
    var siteId, 
        categories = [/*
    	{
    	categoryName: "News",
    	categoryFeeds: [ {id:0, title:'test'}, {id:0, title:'test'} ]
    	}
    */];
        feeds = [
        /*[ {
            id:id, 
            entries:[ {object}, {object} ] 
            } ]
        */
        ];        
    
    function getParsedFeed(url) {
        return $http.post('/getParsedFeed', {url: url}).then(function(responce) {            
            console.log(responce.data);
            return responce.data;
        });        
    }

    function saveFeed(title, categoryName) {
        return $http.post('/addFeed', {                
                title: title, 
                category: categoryName 
            })
            .then(function(res) {           
                //console.log(title, categoryName, res);     
                if(res.data.status == 'exist'){
                    alert(res.data.msg);    
                } else {
                    console.log(res);
                    getSavedData();
                }                
                //$rootScope.$broadcast('data_saved');
                return res;                
            },
            function(error) {
                alert('Can not save feed');
            })
    }

    function getSavedData() {
        return $http.get('/getSidebarList').then(function(responce) {                            
            categories = responce.data.sidebar;                
            //console.log(responce); 
        });
    } 

    function getCategoryArray(){        
        return categories;
    }

    function getFeedsArray(){        
        return feeds;
    }

    function getFeedById(id){           
        return $http.post('/getFeedBySiteId', { id: id }).then(function(res) {                
                feeds = res.data;                
            },
            function(error) {
                alert('Can not get feed');
            })
    }

    function shuffle(arr) {
        return arr.sort(function() {return 0.5 - Math.random()});
    }    

    function getShuffledFeeds(){
        return shuffle(feeds);
    }

    function getFeedEntryById(entryId){            
        var entry = null
        feeds.forEach(function(item){
            if(item.id == entryId){
                entry = item
            }                   
        });
        return entry.entry;        
    }

    function removeFeedById(feedId){
        categories.forEach(function(item, i, arr){
            var cat = item.categoryFeeds;
            cat.splice(feedId,1);            
        });        
        feeds.splice(feedId, 1);        
    }

    return {
        getParsedFeed      : getParsedFeed,        
        getCategoryArray   : getCategoryArray,
        getFeedsArray      : getFeedsArray,
        getFeedById        : getFeedById,
        getFeedEntryById   : getFeedEntryById,
        removeFeedById     : removeFeedById,
        saveFeed           : saveFeed,
        getSavedData       : getSavedData,        
        getShuffledFeeds   : getShuffledFeeds
    }
}])
