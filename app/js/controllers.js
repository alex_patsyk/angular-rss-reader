'use strict';

//News feed controller
rssReaderApp.controller('FeedLayoutCtrl', ['$scope','$stateParams','$location','feedService', function($scope, $stateParams,$location, feedService){    
    $scope.category = $stateParams.cat;    
    $scope.removeFeedById = function(){
        //console.log('sadf');
        feedService.removeFeedById($scope.category);
        $location.path('dashboard');
    }
}])

//News feed controller
rssReaderApp.controller('FeedListCtrl', ['$scope','$stateParams', 'feedService', function($scope, $stateParams, feedService){
    $scope.category = $stateParams.cat; 
    /*$scope.feedLayoutChange = function(){
        console.log('sdf');
    }*/

    feedService.getFeedById($stateParams.cat).then(function(){        
        $scope.news = feedService.getFeedsArray();
        console.log($scope.news);
    });

    /*feedService.getFeedTest($stateParams.cat).then(function(){        
        $scope.news = feedService.getFeedsArray();
        console.log($scope.news);
    });*/

}])

//Single page controller
rssReaderApp.controller('SingleNewsCtrl', ['$scope','$stateParams','feedService', function($scope, $stateParams, feedService){
    $scope.category = $stateParams.cat;
    var entry = feedService.getFeedEntryById($stateParams.entry);
    console.log(entry);
    if(entry.length != 0){
        $scope.single = entry;        
    }

    $scope.similar = feedService.getShuffledFeeds();
    console.log($scope.similar);

}])

//Sidebar page controller
rssReaderApp.controller('SidebarCtrl', ['$scope','$rootScope', 'feedService', function($scope, $rootScope, feedService){   
    $scope.titles = [];    
    $scope.$watch(function(){
        $scope.titles = feedService.getCategoryArray();
        return $scope.titles;
    }, true);

}])

//Dashboard page controller
rssReaderApp.controller('DashboardCtrl', ['$scope', '$rootScope', '$location', 'feedService', function($scope, $rootScope, $location, feedService){    
    $scope.feed = '';
    function loadData() {
        feedService.getSavedData();        
    }

    loadData();            

    $scope.getFeedArticle = function() {
        feedService.saveFeed($scope.feedLink, $scope.feedCategory)
            .then(function(responce){                
                console.log(responce);                
        });        
    }
    
}])
