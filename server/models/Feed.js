var mongoose = require('mongoose');

/*var feedSchema = new mongoose.Schema({
	title: { type: String, unique: true },
	category : {type: String},
	entries: { type: Array }
});*/

var feedSchema = new mongoose.Schema({
	feedurl: { type: String, unique: true },
	sitetitle: { type: String},
	category : {type: String},
	date: {type: String}
});

module.exports = mongoose.model('Feed', feedSchema);

var favoriteFeedSchema = new mongoose.Schema({
	feedurl: { type: String, unique: true },
	sitetitle: { type: String},
	category : {type: String},
	date: {type: String}
});

module.exports = mongoose.model('FavoriteFeed', favoriteFeedSchema);