var mongoose = require('mongoose'),
	fs = require("fs"),
	FeedParser = require('feedparser'),
    request    = require('request'),
    Promise    = require('promise'),
	Feed = mongoose.model('Feed');

function getParsedFeed(url) {
    return new Promise(function (resolve, reject){
        var feedparser = new FeedParser({ normalize : true, addmeta: true  });
        url.on('error', function (error) { console.log('Feed xml req error')});
        url.on('response', function (res) {
            var stream = this;
            if (res.statusCode !== 200) {
                this.emit('error', new Error('Bad status code'));
            } else {
                stream.pipe(feedparser);
            }
        });

        feedparser.on('error', function (error) { console.log("FeedParser error");});
        var parsedFeed = [];
        feedparser.on('readable', function () {
            var stream = this,
            meta = this.meta,
            item;
            while (item = stream.read()) {
                parsedFeed.push(item);
            }
        });
        feedparser.on('end', function() {
            //resolve(createFeedsArray(parsedFeed,category))
            resolve( parsedFeed );
        });
    });
}

module.exports.addFeed = function(req, res) {
            
        Feed.findOne({ feedurl: req.body.title }, function(error, feed){
            if(feed) {
                //getParsedFeed(request(req.body.title), req.body.category ).then(function(data){
                    //return res.send(feed);
                //});
                
                return res.send({status:'exist', msg:'Entered url already exists.'});
                            
            } else {
                getParsedFeed(request(req.body.title), req.body.category ).then(function(data){
                    var feed = new Feed();
                    feed.feedurl = req.body.title;
                    feed.sitetitle = data[0].meta.title;
                    feed.category = req.body.category;
                    feed.date = new Date();
                    feed.save(function(err){
                        if (err) {
                            return res.send({
                                err: err
                            })
                        }                        
                        return res.send({status:'added'});
                    });                    
                });                
            }
        });    
}


module.exports.getSidebarList = function(req, res) {    
    var newsEntries = [],
        sidebarEntries = [],
        feed = null;    

    function buildSidebarArray(data){
        var cat = [];            

        data.forEach(function(item, i){
            cat.push(item.category);
        });

        var unique = cat.filter(function(item, i, ar){ 
            return ar.indexOf(item) === i; 
        });
        
        unique.forEach(function(cat_item){
            var sites = [];
            data.forEach(function(data_item){                
                if(cat_item === data_item.category){
                    sites.push({ id:data_item._id, title:data_item.sitetitle });
                }
            })

            sidebarEntries.push({categoryName:cat_item, categoryFeeds:sites });
        });            
    }
        
    Feed.find(function(error, cursor){        
        buildSidebarArray(cursor);        
        return res.send({sidebar:sidebarEntries});
    });    
}

function getImageSrc(item) {
    var src;
    switch (true) {
        case "rss:enclosure" in item :
            src = item.enclosures[0].url;
            break;
        case "media:content" in item || "media:thumbnail" in item :
            src = item.image.url;
            break;
        default:
            try {
                var description = item.description;
                var regex = /img src\s*=\s*"(.+?)"/;
                src = regex.exec(description)[1];
            }
            catch (e){
                if (src == "" || typeof (src) == 'undefined') {
                    src = "./img/default-image.jpg";
                }
            }
    }
    return src;
}

function prepareFeedsArray(data){
    var feeds = []
    data.forEach(function(item, i){
        feeds.push({
            id:i,
            entry:{
                link    :item.link,
                title   :item.title,
                img     : getImageSrc(item),
                content : item.summary,
                date    :item.date,
            }    
        });
    });
    return feeds;
}

module.exports.getFeedBySiteId = function(req, res) {
    Feed.findOne({ _id: req.body.id }, function(error, feed){
        getParsedFeed(request(feed.feedurl)).then(function(data){
            return res.send(prepareFeedsArray(data));            
        });
    })    
}
